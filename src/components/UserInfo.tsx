import React from 'react';
import { Form, Input, Button, DatePicker, TimePicker } from 'antd';
import { IUserInfo } from '../interfaces';

const { TextArea } = Input;

export const UserInfo = (props: { callback: Function }) => {
  const [form] = Form.useForm();
  const onFinish = (values: IUserInfo) => {
    const res = {
      ...values,
      appointmentDate: values.appointmentDate.format('DD/MM/YYYY'),
      appointmentHour: values.appointmentHour.format('hh:mm a'),
    };
    form.resetFields();
    props.callback(res);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="First Name:"
        name="firstName"
        rules={[
          {
            required: true,
            message: 'Please add your first name...',
          },
        ]}
      >
        <Input placeholder="Add your First Name" />
      </Form.Item>

      <Form.Item
        label="Last Name:"
        name="lastName"
        rules={[
          { required: true, message: 'Please, add your last name' },
        ]}
      >
        <Input placeholder="Add your Last Name" />
      </Form.Item>

      <Form.Item
        label="Occupation:"
        name="occupation"
        rules={[
          { required: true, message: 'Please, add your occupation' },
        ]}
      >
        <Input placeholder="Add your Occupation" />
      </Form.Item>

      <Form.Item
        label="Set the Date"
        name="appointmentDate"
        rules={[
          {
            required: true,
            message: 'Please, pick a date for your appointment',
          },
        ]}
      >
        <DatePicker format="DD/MM/YYYY" />
      </Form.Item>
      <Form.Item
        label="Set the Time"
        name="appointmentHour"
        rules={[
          {
            required: true,
            message: 'Please, pick an hour for your appointment',
          },
        ]}
      >
        <TimePicker format="hh:mm a" />
      </Form.Item>
      <Form.Item
        name="symptoms"
        rules={[
          {
            required: true,
            message: 'Please, add all your symptoms...',
          },
        ]}
      >
        <TextArea
          rows={5}
          placeholder="Add your symptoms..."
        ></TextArea>
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};
