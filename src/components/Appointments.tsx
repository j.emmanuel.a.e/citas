import React, { Fragment } from 'react';
import { IUserInfo } from '../interfaces';
import { Button, Card, Row } from 'antd';

export const Appointments = (props: {
  data: IUserInfo[];
  callback: Function;
}) => {
  // const {
  //   firstName,
  //   lastName,
  //   occupation,
  //   appointmentDate,
  //   appointmentHour,
  //   symptoms,
  // } = props.data;

  const { data, callback } = props;

  return (
    <Fragment>
      {data.map((user: IUserInfo, index: number) => {
        const {
          clientNumber,
          firstName,
          lastName,
          occupation,
          appointmentDate,
          appointmentHour,
          symptoms,
        } = user;

        let t = `Patient Number #${clientNumber}`;
        return (
          <Card key={index} title={t} style={{ width: 300 }}>
            <p> {`First Name: ${firstName}`}</p>
            <p> {`Last Name: ${lastName}`}</p>
            <p> {`Occupation: ${occupation}`}</p>
            <p>{`Appointment: ${appointmentDate} ${appointmentHour}`}</p>
            <p> {`Symptoms: ${symptoms}`}</p>
            <Row justify="end">
              <Button type="dashed" onClick={() => callback(index)}>
                Done
              </Button>
            </Row>
          </Card>
        );
      })}
    </Fragment>
  );

  // return (
  //   <Card title="Patient Number #1" style={{ width: 300 }}>
  //     <p> {`First Name: ${firstName}`}</p>
  //     <p> {`Last Name: ${lastName}`}</p>
  //     <p> {`Occupation: ${occupation}`}</p>
  //     <p>{`Appointment: ${appointmentDate} ${appointmentHour}`}</p>
  //     <p> {`Symptoms: ${symptoms}`}</p>
  //   </Card>
  // );
};
