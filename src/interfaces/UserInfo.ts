import { Moment } from 'moment';

export interface IUserInfo {
  clientNumber: number;
  firstName: string;
  lastName: string;
  occupation: string;
  appointmentDate: Moment;
  appointmentHour: Moment;
  symptoms: string;
}
