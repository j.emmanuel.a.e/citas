import React, { useState, Fragment } from 'react';
import { Layout, Row, Col } from 'antd';
import { UserInfo, Appointments } from './components';
import { IUserInfo } from './interfaces';

function App() {
  const [data, setData] = useState<IUserInfo[]>([]);

  const callback = (values: IUserInfo) => {
    const lastElement = data[data.length - 1];

    const nextClientNumber = lastElement
      ? lastElement.clientNumber + 1
      : 1;

    data.push({ ...values, clientNumber: nextClientNumber });
    setData([...data]);
  };

  const deleteUser = (index: number) => {
    data.splice(index, 1);
    setData([...data]);
  };

  return (
    <Layout>
      <Layout.Header color="white"></Layout.Header>

      <Row justify="center" style={{ minHeight: '100vh' }}>
        <Col span={12}>
          <h2>Set the Appointment</h2>
          <UserInfo callback={callback} />
        </Col>
        <Col span={12}>
          {data && (
            <Fragment>
              <Col offset={4}>
                <Row>
                  <h1>Appointment</h1>
                </Row>
                <Row>
                  <Appointments data={data} callback={deleteUser} />
                </Row>
              </Col>
            </Fragment>
          )}
        </Col>
      </Row>
    </Layout>
  );
}

export default App;
